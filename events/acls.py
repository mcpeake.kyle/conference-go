from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):
    # Use the Pexels API
    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state

    # TODO go through documentation to search photos, plug in City / State
    # url = f'https://api.pexels.com/v1/search?query={city},%20{state}&per_page=1'
    headers = {'Authorization': PEXELS_API_KEY}

    # TEACHER'S SOLUTION
    params = {
        "query": f'{city}, {state}',
        "per_page": '1'
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    try:
        content = response.json()
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError, requests.exceptions.JSONDecodeError):
        return {"picture_url": None}

    # # Make the request
    # r = requests.get(url, headers=headers)
    # # Parse the JSON response
    # r.json()
    # # Return a dictionary that contains a `picture_url` key and
    # #   one of the URLs for one of the pictures in the response
    # d = {
    #     "url": r["url"]
    # }
    # return d


def get_weather_data(city, state):
    # Use the Open Weather API
    # Create the URL for the geocoding API with the city and state
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit={1}&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    r = requests.get(url)
    # Parse the JSON response
    context = r.json()
    print(context)
    # Get the latitude and longitude from the response
    lat = context[0]["lat"]
    lon = context[0]["lon"]

    # Create the URL for the current weather API with the latitude
    #   and longitude
    weather = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    weather_response = requests.get(weather)
    # Parse the JSON response
    content = weather_response.json()
    # Get the main temperature and the weather's description
    # Convert Kelvin to Fahrenheit
    temperature = round((content["main"]["temp"] - 273.15) * 9/5 + 32)
    # put them in a dictionary
    d = {
        "temperature": f"{temperature}\xb0F",
        "weather description": content["weather"][0]["description"],
    }
    # Return the dictionary
    return d
